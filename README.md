# csv2svg

Un outil simple avec interface graphique pour générer des rectangles dans un fichier SVG à partir d'une feuille de calcul CSV

![](img/img1.png)
![](img/img2.png)

Un modèle de fichier CSV est disponible dans le répertoire 'exemples" de ce dépôt.

À l'importation du CSV, la première ligne n'est pas utilisée, elle ne sert qu'aux étiquettes des colonnes pour lesquelles il faudra respecter l'ordre des données.

Cet outil pourra être pratique pour la réalisation de guides pour personnes ayant une déficience visuelle (guides chèques, guide signature, guide enveloppe...) qui seront découpés au laser.

Une version compilée pour Windows est [téléchargeable ici](https://git.lolivz.ovh/attachments/fde2611c-8a6f-47b2-a2fd-4054ddfa9ccd).
